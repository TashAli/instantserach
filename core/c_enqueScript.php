<?php

function enqueu_searchwp_scripts()
	{
		
		wp_enqueue_script('deliver-js', 'https://cdn.jsdelivr.net/npm/instantsearch.js@2.5.1',false,true);		
		wp_enqueue_style('instant-style', 'https://cdn.jsdelivr.net/npm/instantsearch.js@2.5.1/dist/instantsearch.min.css');
		wp_enqueue_style('search-css', plugins_url('../assets/css/wp-search.css',__FILE__));
		
		
	}
	
		add_action('wp_enqueue_scripts','enqueu_searchwp_scripts');
		add_action( 'admin_enqueue_scripts', 'enqueu_searchwp_scripts' );
		
	?>	