<script>
jQuery(document).ready(function($){
	
const search = instantsearch({
  appId: '<?php echo get_option('its_appid');?>',
  apiKey: '<?php echo get_option('its_appkey');?>',
  indexName: '<?php echo get_option('its_indexname');?>',
  urlSync: true
});



   var refinmentlist='<?php echo get_option('its_refinmentlist');?>';
   if(refinmentlist== true)
   {
	   search.addWidget(
		instantsearch.widgets.refinementList({
		  container: '#refinement-list',
		  attributeName: 'categories'
		})
	  );
   }

  // initialize SearchBox
  search.addWidget(
    instantsearch.widgets.searchBox({
      container: '#search-box',
      placeholder: 'Search for products'
    })
  );

  // initialize hits widget
  search.addWidget(
    instantsearch.widgets.hits({
      container: '#hits',
	  hitsPerPage:10,
	  templates:{
		  item:document.getElementById('hit-template').innerHTML,
		  empty:"We didn't find any results for the search <em>\"{{query}}\"</em>",
	  }
    })
  );
  
  search.addWidget(
    instantsearch.widgets.pagination({
      container: '#pagination',
      maxPages: 10,
      // default is to scroll to 'body', here we disable this behavior
      scrollTo: false
    })
  );
  search.addWidget(
    instantsearch.widgets.currentRefinedValues({
      container: '#current-refined-values',
      // This widget can also contain a clear all link to remove all filters,
      // we disable it in this example since we use `clearAll` widget on its own.
      clearAll: false
    })
  );
 search.addWidget(
    instantsearch.widgets.clearAll({
      container: '#clear-all',
      templates: {
        link: 'Reset everything'
      },
      autoHideContainer: false
    })
  );
  search.start();
});
</script>