<?php
/**
 * Plugin Name: WP Search
 * Plugin URI: #
 * Description: Live Wordpress Search plugin.
 * Author: Intelligic
 * Author URI: #
 * Version: 1.0.0
 * Text Domain: wpsearch
 * Domain Path: /languages
 * Network: True
 *
*/
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
require_once( ABSPATH . "wp-includes/pluggable.php" );
require_once(  plugin_dir_path( __FILE__ ). 'core/c_enqueScript.php' );
require_once(  plugin_dir_path( __FILE__ ). '_inc/searchform.php' );
require_once(  plugin_dir_path( __FILE__ ). '_inc/searchpage.php' );


?>


