<?php

 add_shortcode('wp_instant_search','instant_search_func');
function instant_search_func()
{
  require_once(  plugin_dir_path( __FILE__ ). '../core/instantsearch.php' );	
	?>


	
<div id="search-box" style="padding-bottom:5%;padding-top:5%">
  <!-- SearchBox widget will appear here -->
</div>
<?php if(get_option('its_refinmentlist')==true){?>
<div id="refinement-list">
  <!-- RefinementList widget will appear here -->
</div>
<?php } ?>

<div id="hits">
  <!-- Hits widget will appear here -->
</div>
<div id="current-refined-values">
  <!-- CurrentRefinedValues widget will appear here -->
</div>

<div id="clear-all">
  <!-- ClearAll widget will appear here -->
</div>
<div id="pagination">
  <!-- Pagination widget will appear here -->
</div>
<br/><br/>
<script id="hit-template">
<table>
<tr >

<img src="{{image}}" >
</tr>
<tr>
<td >${{price}}</td>
</tr>
</tr>
<td>{{_highlightResult.name.value}}</td>
<td style="border-left:3px solid ">{{_highlightResult.description.value}}</td>

</tr>
</table>
</script>


<?php } ?>