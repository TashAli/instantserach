<?php


function search_plugin_setup_menu()
{
	add_menu_page( 'InstantSearch', 'InstantSearch' , 'manage_options', 'instant-search', 'instant_search',plugins_url('../assets/images/menu.PNG',__FILE__) ,11);
	add_submenu_page( 'instant-search', 'Admin Search', 'Admin Search', 'manage_options', 'adminsearch','admin_search_init');
}
add_action('admin_menu', 'search_plugin_setup_menu');
function instant_search()
{
if(isset($_POST['paysave']))
{
    update_option('its_appid',$_POST['its_appid']);
    update_option('its_appkey',$_POST['its_appkey']);
    update_option('its_indexname',$_POST['its_indexname']);
    update_option('its_refinmentlist',$_POST['its_refinmentlist']);
}


?>
     <form method="post" class="nothing">
			   <h2> Instant Search Settings </h2>
				
				 
				 APP ID: &nbsp <input name="its_appid" style="margin-left:6%" id="its_appid" type="text" value="<?php echo get_option('its_appid'); ?>">
				  <br/>
				  <br/>
				 APP KEY:<input name="its_appkey" id="its_appkey" type="text"  style="margin-left:6%" value="<?php echo get_option('its_appkey'); ?>">
				  <br/>
				  <br/>
				   INDEX NAME:<input name="its_indexname" style="margin-left:4%" id="its_indexname" type="text" value="<?php echo get_option('its_indexname'); ?>">
				  <br/>
				  <br/>
				  <input name="its_refinmentlist" id="its_refinmentlist" type="checkbox"   <?php if(get_option('its_refinmentlist')==true){echo "checked";} ?>> Allow Refinement List 
				  <br/>
				  <br/>
				 
				  <input id="submit" name="paysave" class="button-primary" type="submit" value="SAVE">
				  
		</form>	
		<p>Use Shortcode To Display Instant Search [wp_instant_search]</p>
	
<?php	
}

function admin_search_init()
{
	
	require_once(  plugin_dir_path( __FILE__ ). '../core/instantsearch.php' );	
	?>


<div id="search-box" style="padding-bottom:5%;padding-top:5%">
  <!-- SearchBox widget will appear here -->
</div>
<?php if(get_option('its_refinmentlist')==true){?>
<div id="refinement-list">
  <!-- RefinementList widget will appear here -->
</div>
<?php } ?>

<div id="hits">
  <!-- Hits widget will appear here -->
</div>
<div id="current-refined-values">
  <!-- CurrentRefinedValues widget will appear here -->
</div>

<div id="clear-all">
  <!-- ClearAll widget will appear here -->
</div>
<div id="pagination">
  <!-- Pagination widget will appear here -->
</div>
<br/><br/>
<script id="hit-template">
<table style="border-collapse: collapse;margin: 0 0 1.5em;width: 100%;">
<tr >

<img src="{{image}}" >
</tr>
<tr>
<td >${{price}}</td>
</tr>
</tr>
<td>{{_highlightResult.name.value}}</td>
<td style="border-left:3px solid ">{{_highlightResult.description.value}}</td>

</tr>
</table>
</script>
<?php
}
?>